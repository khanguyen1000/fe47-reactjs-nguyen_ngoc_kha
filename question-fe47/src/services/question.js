import Axios from 'axios';

class Question {
    getAllQuestion() {
        return Axios({
            method: "get",
            url: 'https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions'
        });
    }
}
export default Question;