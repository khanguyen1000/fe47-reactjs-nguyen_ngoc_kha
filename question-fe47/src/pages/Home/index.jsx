import React, { useCallback, useEffect } from 'react';
import styled from './style';
import { Box, Button, Card, CardActions, CardContent, FormControlLabel, FormLabel, Grid, Radio, RadioGroup, TextField, Typography, withStyles } from '@material-ui/core';
import QuesText_item from '../../components/QuesText_item';
import QuestionCheck from '../../components/QuesC_item';
import { useDispatch, useSelector } from 'react-redux';
import { getListQuestion } from '../../redux/action/questionAction';
import { createAction } from '../../redux/action';
import { CHECK_RESULTS } from '../../redux/action/type';
const Home = (props) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getListQuestion());
    }, []);
    const listQCheck = useSelector((state) => {
        return state.qlQuestion.listQCheck
    });
    const listQText = useSelector((state) => {
        return state.qlQuestion.listQText
    })
    const renderListQcheck = useCallback(() => {
        return listQCheck.map((item, index) => {
            return (
                <QuestionCheck index={index + 1} item={item} key={index} />
            )
        })
    }, [listQCheck]);

    const renderListQText = useCallback(() => {
        return listQText.map((item, index) => {
            return (
                <QuesText_item item={item} key={index} />
            )
        })
    }, [listQText]);
    const handleClickSubmit = useCallback(() => {
        dispatch(createAction(CHECK_RESULTS, {}));
    })

    return (
        <Box className={props.classes.page}>
            <Box>
                <Typography component="p" className={props.classes.titlePage}>Questions Game</Typography>
                <Card variant="outlined" className={props.classes.card}>
                    <CardContent>
                        {/* <Box display="flex" justifyContent="flex-end">
                            <Button variant="contained" color="primary" className="btnSubmit">
                                Nộp bài
                            </Button>
                        </Box> */}
                        {/* <Typography color="textSecondary">
                            1/13
                        </Typography> */}
                        <Box>
                            {renderListQcheck()}
                            {renderListQText()}

                        </Box>


                    </CardContent>
                    <CardActions className={props.classes.groupCardAction}>
                        {/* <Button size="small" variant="outlined" color="secondary">Câu trước</Button>
                        <Button size="small" variant="outlined" color="primary">Tiếp theo</Button> */}
                        <Box display="flex" justifyContent="flex-end">
                            <Button variant="contained" color="primary" className="btnSubmit"
                                onClick={handleClickSubmit}
                            >
                                Nộp bài
                            </Button>
                        </Box>
                    </CardActions>
                </Card>
            </Box>
        </Box>
    );
};

export default withStyles(styled)(Home);