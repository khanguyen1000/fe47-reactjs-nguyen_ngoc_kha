const styled = () => {
    return {
        page: {
            backgroundImage: 'linear-gradient(to right, rgb(198, 255, 221), rgb(251, 215, 134), rgb(247, 121, 125))',
            minHeight: '800px',
            height: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            padding: '20px 0',
        },
        titlePage: {
            color: '#fff',
            letterSpacing: '3px',
            fontSize: 35,
            textAlign: 'center',
        },
        card: {
            width: '800px',
            minHeight: '350px',
            height: 'auto',
            textAlign: 'center',
            position: 'relative',
            borderRadius: 20,
            '& .btnSubmit': {
                borderRadius: 5,
            }
        },
        quesTitle: {
            color: '#071a52',
            '& .title': {
                letterSpacing: '3px !important',
                fontWeight: '600',
            },
            '& .title_group': {
                letterSpacing: '2px !important',
                fontWeight: '400',
                margin: '10px 0',
            },
        },
        quesAnswersCheck: {
            minHeight: '200px',
            marginBottom: '10%',
            '& .title': {
                letterSpacing: '3px !important',
                fontWeight: '400',
                color: '#574f7d',
            },
        },
        quesAnswersText: {
            minHeight: '100px',
            marginTop: '5%',
            marginBottom: '5%',
            '& .input': {
                width: '100%',
            },
        },
        groupCardAction: {
            position: 'absolute',
            margin: '10px 0',
            bottom: 0,
            right: 0,
            left: 0,
            justifyContent: 'flex-end',
            padding: '16px !important',
        }

    }
}
export default styled;