import swal from 'sweetalert';
import { CHECK_RESULTS, SET_DATA_LIST_QUESTION, SET_LIST_ANSWERS } from '../action/type';

let initialState = {
    // listQuestion: [],
    listQCheck: [],
    listQText: [],
    listAnswers: []
};

const QuestionReducer = (state = initialState, { payload, type }) => {
    switch (type) {
        case SET_DATA_LIST_QUESTION: {
            // state.listQuestion = payload;
            state.listQCheck = payload.filter(q => q.questionType === 1);
            state.listQText = payload.filter(q => q.questionType === 2);
            return { ...state };
        } case SET_LIST_ANSWERS: {
            let index = state.listAnswers.findIndex(aw => aw.questionId === payload.questionId);
            if (index !== -1) {
                let mangmoi = [...state.listAnswers];
                mangmoi[index] = payload;
                state.listAnswers = mangmoi;
            } else {
                state.listAnswers = [...state.listAnswers, payload];
            }

            return { ...state };
        } case CHECK_RESULTS: {
            let results = 0;
            for (let item of state.listAnswers) {
                item.anwser.exact && results++;
            }
            if (results !== 0) {
                swal({
                    title: `Chúc mừng bạn`,
                    text: `bạn có ${results} đáp án đúng`,
                    icon: 'success'
                });
            } else {
                swal({
                    title: `Èo !!!`,
                    text: `bạn gà vl !`,
                    icon: 'success'
                });
            }
            return { ...state };
        }
        default: {
            return state;
        }
    }
};
export default QuestionReducer;

