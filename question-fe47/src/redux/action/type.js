export const SET_DATA_LIST_QUESTION = 'question/SET_DATA_LIST_QUESTION';

export const SET_LIST_ANSWERS = 'question/SET_LIST_ANSWERS';

export const CHECK_RESULTS = 'question/CHECK_RESULTS';