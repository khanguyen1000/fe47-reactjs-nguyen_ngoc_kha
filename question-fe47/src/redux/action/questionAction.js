import { Question } from '../../services';
import { createAction } from './';
import { SET_DATA_LIST_QUESTION } from './type';
export const getListQuestion = () => {
    return dispatch => {
        Question.getAllQuestion().then(res => {
            dispatch(createAction(SET_DATA_LIST_QUESTION, res.data));
        }).catch(err => {
            console.log(err);
        })
    }
}

// export const checkResults = (callback)=>{

// }