const styled = () => {
    return {
        quesTitle: {
            color: '#071a52',
            '& .title': {
                letterSpacing: '3px !important',
                fontWeight: '600',
            },
            '& .title_group': {
                letterSpacing: '2px !important',
                fontWeight: '400',
                margin: '10px 0',
            },
        },
        quesAnswersCheck: {
            minHeight: '200px',
            marginBottom: '10%',
            '& .title': {
                letterSpacing: '3px !important',
                fontWeight: '400',
                color: '#574f7d',
            },
        },
        quesAnswersText: {
            minHeight: '100px',
            marginTop: '5%',
            marginBottom: '5%',
            '& .input': {
                width: '100%',
            },
        },
    }
}
export default styled;