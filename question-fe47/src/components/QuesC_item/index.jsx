import { Box, FormControlLabel, Grid, Radio, RadioGroup, Typography, withStyles } from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { SET_LIST_ANSWERS } from '../../redux/action/type';
import styled from './style';
import { createAction } from '../../redux/action';
const QuestionCheck = (props) => {
    const [answer, setAnswer] = useState('');
    const dispatch = useDispatch();
    const handleChange = useCallback((item) => (e) => {
        setAnswer(item.exact);
        dispatch(createAction(SET_LIST_ANSWERS, { questionId: props.item.id, questionType: props.item.questionType, anwser: { exact: item.exact } }));
    }, [answer]);
    const renderListAnswers = useCallback(() => {
        return props.item.answers.map((item, index) => {
            return (
                <Grid item xs={6} key={index}>
                    <Box width="100%" textAlign="left">
                        <FormControlLabel value={item.id} name={'cautraloi' + props.item.id} onChange={handleChange(item)} className="quests" control={<Radio />} label={item.content} />
                    </Box>
                </Grid>
            )
        })
    }, [props.item.answers]);


    return (
        <div>
            <Box className={props.classes.quesTitle}>
                <Typography component="p" align="left" className="title">
                    câu {props.item.id} :
                </Typography>

                <Typography component="p" align="center"
                    className="title_group"
                >
                    {props.item.content}
                </Typography>
            </Box>
            <Box className={props.classes.quesAnswersCheck}>
                <Typography color="textPrimary" align="left" className="title">câu trả lời :</Typography>
                <RadioGroup name={'cau' + props.item.id} >
                    <Box px={2} py={2}>
                        <Grid container spacing={1}>
                            {renderListAnswers()}
                        </Grid>
                    </Box>
                </RadioGroup>
            </Box>
        </div>
    );
};

export default withStyles(styled)(QuestionCheck);
