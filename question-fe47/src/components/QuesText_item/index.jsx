import { Box, TextField, Typography, withStyles } from '@material-ui/core';
import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { createAction } from '../../redux/action';
import { SET_LIST_ANSWERS } from '../../redux/action/type';
import styled from './style';
const QuestionText = (props) => {
    const [text, setText] = useState('');
    const dispatch = useDispatch();
    const handleChange = useCallback((item) => (e) => {
        setText(e.target.value);
        let exact = item.answers[0].content.toLowerCase() === e.target.value.toLowerCase();
        dispatch(createAction(SET_LIST_ANSWERS, { questionId: item.id, questionType: item.questionType, anwser: { content: e.target.value, exact: exact } }));
    }, [text]);
    return (
        <div>
            <Box className={props.classes.quesAnswersText}>
                <Typography component="p" align="left" className="title">
                    câu {props.item.id} :
                </Typography>

                <Typography component="p" align="center"
                    className="title_group"
                >
                    {props.item.content}
                </Typography>
            </Box>
            <Box className={props.classes.quesAnswersText}>
                <TextField label="Đáp án của bạn " value={text} type="text" onChange={handleChange(props.item)} className="input" />
            </Box>
        </div>
    );
};

export default withStyles(styled)(QuestionText);
