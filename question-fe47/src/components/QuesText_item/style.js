const styled = () => {
    return {
        quesAnswersText: {
            minHeight: '100px',
            marginTop: '5%',
            marginBottom: '5%',
            '& .input': {
                width: '100%',
            },
            '& .title_group': {
                letterSpacing: '2px !important',
                fontWeight: '400',
                margin: '10px 0',

            },
            '& .title': {
                letterSpacing: '3px !important',
                fontWeight: '600',
                color: '#071a52'
            },
        },
    }
}
export default styled;