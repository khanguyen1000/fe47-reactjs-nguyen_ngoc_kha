import bg from '../../assets/img/backGroup.svg';
const styled = (theme) => {
    return {
        Page: {
            backgroundImage: `url(${bg})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            minHeight: '1000px',
            height: 'auto',
        },
        titlePage: {
            color: '#F4FFFD',
            textTransform: 'uppercase',
        },
    }
}
export default styled;