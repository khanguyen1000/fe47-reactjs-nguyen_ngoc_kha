import React from 'react';
import styled from './style';
import { Grid, Button, Typography, withStyles, Box } from '@material-ui/core';
import ListSeat from '../../components/ListSeat';
import ListSeatBooked from '../../components/ListSeatBooked';
const HomeComponent = (props) => {
    return (
        <div className={props.classes.Page}>
            <Box marginBottom={2} padding="10px 0">
                <Typography
                    align="center"
                    variant="h3"
                    component="h3"
                    className={props.classes.titlePage}
                >Đặt vé xe bus hãng CYBERSOFT</Typography>
            </Box>
            <Grid container>
                <Grid item xs={12} md={6}>
                    <ListSeat />
                </Grid>
                <Grid item xs={12} md={6}>
                    <ListSeatBooked />
                </Grid>
            </Grid>

        </div>

    );
};

export default withStyles(styled, { withTheme: true })(HomeComponent);