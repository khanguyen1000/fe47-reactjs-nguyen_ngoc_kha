import { Box, Button, Grid, Typography, withStyles } from '@material-ui/core';
import React, { useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import SeatItem from '../SeatItem';
import styled from './style';
const ListSeat = (props) => {
    const listSeat = useSelector((state) => {
        return state.seatManage.listSeat
    })
    const renderListSeat = useCallback(() => {
        return listSeat.map((item, index) => {
            return (
                <SeatItem item={item} key={index} />
            )
        })
    }, [listSeat]);
    return (
        <Box p={3}>
            <Box my={3}>
                <Typography
                    align="center"
                    variant="h4"
                    component="h6"
                    className={props.classes.titlePage}
                >
                    Tình trạng Ghế
                        </Typography>
            </Box>
            <Grid container spacing={2} justify="center" alignItems="center">
                <Grid item xs={12}>
                    <Button variant="contained" className={props.classes.buttonTaiXe}>Tài xế</Button>
                </Grid>
            </Grid>
            <Grid container spacing={2} justify="flex-start" alignItems="center">
                {renderListSeat()}
            </Grid>
        </Box>
    );
};

export default withStyles(styled, { withTheme: true })(ListSeat);