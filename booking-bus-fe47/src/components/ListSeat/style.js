const styled = (theme) => {
    return {
        titlePage: {
            color: '#F4FFFD',
            textTransform: 'uppercase',
        },
        buttonTaiXe: {
            width: '100%',
            color: '#011936',
            letterSpacing: '3px',
            padding: '20px',

        }
    }
}
export default styled;