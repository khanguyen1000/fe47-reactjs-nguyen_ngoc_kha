import { Box, Button, Grid, Typography, withStyles } from '@material-ui/core';
import React, { useCallback, useState, memo } from 'react';
import styled from './style';
import swal from 'sweetalert';
import { useDispatch } from 'react-redux';
import { createAction } from '../../redux/action';
import { SET_VALUE_STATUS_SEAT } from '../../redux/action/type';
import EventSeatIcon from '@material-ui/icons/EventSeat';
const SeatItem = (props) => {
    const dispatch = useDispatch();
    const [isChoosed, setIsChoosed] = useState(props.item.TrangThai);
    const handleChooseSeat = useCallback(() => {
        if (!props.item.TrangThai) {
            swal({
                title: "Thành công!",
                text: `Cảm ơn bạn đã chọn ghế ${props.item.TenGhe}!`,
                icon: "success",
            });
            // setIsChoosed(true);
            dispatch(createAction(SET_VALUE_STATUS_SEAT, { value: props.item, isChoosed: true }));
        } else {
            swal({
                title: "Bạn chắc chứ?",
                text: "Bạn có muốn hủy chọn ghế này không!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Ok, chúng tôi đã hủy đặt ghế này cho bạn !", {
                            icon: "success",
                        });
                        // setIsChoosed(false);
                        dispatch(createAction(SET_VALUE_STATUS_SEAT, { value: props.item, isChoosed: false }));
                    } else {
                        swal("Bạn thật sáng suốt!");
                    }
                });
        }
    }, [dispatch, props.item.TrangThai]);
    console.log('render SeatItem');
    return (

        <Grid item xs={3}>
            <Button variant="contained" color={isChoosed ? 'secondary' : (props.item.TrangThai ? 'primary' : 'default')} className={props.classes.buttonSeat} onClick={handleChooseSeat}
                disabled={isChoosed}
            >
                <Box m={3} height={25} width={25} display="flex" justifyContent="center">
                    <Typography component="p">{props.item.SoGhe}</Typography>
                    <EventSeatIcon />
                </Box>
            </Button>
        </Grid>
    );
};

export default (withStyles(styled)(SeatItem));