import { Box, Grid, Typography, withStyles } from '@material-ui/core';
import React, { useCallback, useEffect } from 'react';
import SeatBookedItem from '../SeatBookedItem';
import { useSelector } from 'react-redux';
import styled from './style';
const ListSeatBooked = (props) => {
    const listSeatBooked = useSelector((state) => {
        return state.seatManage.listSeatBooked
    });
    const renderListSeatBooked = useCallback(() => {
        return listSeatBooked.map((item, index) => {
            return (
                <SeatBookedItem index={index + 1} item={item} key={index} />
            )
        })
    }, [listSeatBooked]);
    return (
        <Box p={3}>
            <Box my={3}>
                <Typography
                    align="center"
                    variant="h4"
                    component="h6"
                    className={props.classes.titlePage}
                >
                    Danh sách ghế đặt
                             </Typography>
            </Box>
            <Grid container>
                {renderListSeatBooked()}
            </Grid>
        </Box>
    );
};

export default withStyles(styled, { withTheme: true })(ListSeatBooked);