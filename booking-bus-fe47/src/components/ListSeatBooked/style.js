const styled = () => {
    return {
        titlePage: {
            color: '#F4FFFD',
            textTransform: 'uppercase',
        },
        buttonCancel: {
            color: '#F4FFFD',
        }
    }
}
export default styled;