import { Box, Button, Grid, IconButton, withStyles } from '@material-ui/core';
import React, { memo, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import styled from './style';
import { createAction } from '../../redux/action';
import { SET_VALUE_STATUS_SEAT } from '../../redux/action/type';
import swal from 'sweetalert';
const SeatBookedItem = (props) => {
    const dispatch = useDispatch();
    const handleCancel = useCallback(() => {
        swal({
            title: "Bạn chắc chứ?",
            text: "Bạn có muốn hủy chọn ghế này không!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Ok, chúng tôi đã hủy đặt ghế này cho bạn !", {
                        icon: "success",
                    });
                    // setIsChoosed(false);
                    dispatch(createAction(SET_VALUE_STATUS_SEAT, { value: props.item, isChoosed: false }));
                } else {
                    swal("Bạn thật sáng suốt!");
                }
            });
    }, [props.item, dispatch]);
    return (
        <Grid item xs={12}>
            <Box color="#F4FFFD" >- Ghế: {props.item.TenGhe} ${props.item.Gia}
                <Button variant="text" className={props.classes.buttonCancel} onClick={handleCancel}>
                    [<Box color="#E15D30">
                        Hủy
                    </Box>]
                </Button>
            </Box>
        </Grid>
    );
};

export default memo(withStyles(styled, { withTheme: true })(SeatBookedItem));