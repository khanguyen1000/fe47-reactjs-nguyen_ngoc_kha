import { combineReducers, createStore } from 'redux';
import SeatReducer from '../redux/reducer/seat';

export const RootReducer = combineReducers({
    seatManage: SeatReducer,
});
export const store = createStore(
    RootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)