import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#a7ff83',
            main: '#83e85a',
            dark: '#2cb978',
        },
        background: {
            default: '#e8ffe8',
            grey: {
                main: '#d4ceb0',
            }
        },
        text: {
            primary: '#393c83',
            secondary: '#fd0054',
        }
    },
    typography: {
        h1: {
            fontSize: 50,
            fontWeight: 600,
            letterSpacing: '1px',
        },
        h2: {
            fontSize: 40,
            fontWeight: 600,
            letterSpacing: '1px',
        },
        h4: {
            fontSize: 30,
            fontWeight: 600,
            letterSpacing: '1px',
        }
    },
    spacing: 10,
});

export default theme;