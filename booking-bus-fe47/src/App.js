import React from 'react';
import HomeComponent from './pages/Home';

function App() {
  return (
    <div className="App">
      <HomeComponent />

    </div>
  );
}

export default App;
